package com.example.that_thon.customautocompleteviews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<CountryItem> countryList;
    private AutoCompleteTextView autoCompleteTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fillCountryList();
        AutoCompleteCountryAdapter autoCompleteCountryAdapter =new AutoCompleteCountryAdapter(this,countryList);
        autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.actv);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setAdapter(autoCompleteCountryAdapter);
    }


    private void fillCountryList() {
        countryList = new ArrayList<>();
        countryList.add(new CountryItem("Afghanistan", R.drawable.apple));
        countryList.add(new CountryItem("Albania", R.drawable.banana));
        countryList.add(new CountryItem("Algeria", R.drawable.cherries));
        countryList.add(new CountryItem("Andorra", R.drawable.grapes));
        countryList.add(new CountryItem("Angola", R.drawable.kiwi));
        countryList.add(new CountryItem("Cambodia", R.drawable.mango));
        countryList.add(new CountryItem("Canada", R.drawable.pear));
    }
}
